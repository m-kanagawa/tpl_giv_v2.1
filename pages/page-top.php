<?php /*Template Name: page-top */ ?>
<?php get_header(); ?>

<section class="post_module01">
	<div class="inner">
		<div class="ttl_box">
			<h2 class="ttl no-style"><a href="<?php echo home_url(); ?>/news/">お知らせ<span>一覧</span></a></h2>
		</div>
		<div class="post_module01_in">
		<dl>
        <?php
        $myposts = get_posts('numberposts=3&post_type=news'); //表示数＆カスタム投稿名
        foreach($myposts as $post) :
        ?>
          <dt><?php echo date("Y.m.d", strtotime($post->post_date)); ?></dt>
          <dd>
						<?php
							$days=14; //Newをつける日数
							$today=date_i18n('U'); $entry=get_the_time('U');
							$diff1=date('U',($today - $entry))/86400;
							if ($days > $diff1){echo'<span class="iconNew">New!</span>';}
						?>
            <a href="<?php the_permalink(); ?>">
              <?php //文字制限し、それを超えた場合は「……」を付ける
              if(mb_strlen($post->post_title, 'UTF-8') > 40){
                $title= mb_substr($post->post_title, 0, 40, 'UTF-8');
                echo $title.'……';
              }else{
                echo $post->post_title;
              }
              ?>
            </a>
          </dd>
        <?php endforeach; ?>
        <?php wp_reset_query(); ?>
        </dl>
		</div>
	</div>
</section>


<section class="top-lead">
			<div class="top-lead__inner">
				<h2 class="no-style">h2タイトルタイトルタイトルタイトルタイトル</h2>
				<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
			</div>
		</section>

		<section class="top-cont01">
			<div class="container gutters inner">
				<div class="row">
					<div class="col span_12">
						<h3 class="top-cont01__h3">h3タイトルタイトルタイトルタイトルタイトル</h3>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>
				<div class="row">
					<div class="col span_5">
						<figure>
							<img src="https://placehold.jp/640x480.png?text=images" loading="lazy" alt="">
						</figure>
					</div>
					<div class="col span_7">
						<h4 class="top-cont01__h4">h4タイトルタイトルタイトルタイトルタイトル</h4>
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
						</p>
					</div>
				</div>
			</div>
		</section>

		<section class="top-cont02">
			<div class="container gutters inner">
				<div class="row">
					<div class="col span_3">
						<figure>
							<img src="https://placehold.jp/640x480.png?text=images" loading="lazy" alt="">
						</figure>
						<h3 class="no-style top-cont03__h3">h3タイトルタイトル</h3>
						<p class="btn_more">
							<a href="medical/#a01" class="add_arrow">more</a>
						</p>
					</div>
					<div class="col span_3">
						<figure>
							<img src="https://placehold.jp/640x480.png?text=images" loading="lazy" alt="">
						</figure>
						<h3 class="no-style top-cont03__h3">h3タイトルタイトル</h3>
						<p class="btn_more">
							<a href="medical/#a03" class="add_arrow">more</a>
						</p>
					</div>
					<div class="col span_3">
						<figure>
							<img src="https://placehold.jp/640x480.png?text=images" loading="lazy" alt="">
						</figure>
						<h3 class="no-style top-cont03__h3">h3タイトルタイトル</h3>
						<p class="btn_more">
							<a href="medical/#ingrown" class="add_arrow">more</a>
						</p>
					</div>
					<div class="col span_3">
						<figure>
							<img src="https://placehold.jp/640x480.png?text=images" loading="lazy" alt="">
						</figure>
						<h3 class="no-style top-cont03__h3">h3タイトルタイトル</h3>
						<p class="btn_more">
							<a href="beauty/" class="add_arrow">more</a>
						</p>
					</div>
				</div>
			</div>
		</section>



  <!-- <section class="top_cont02">
    <div class="inner">
    <?php //if (have_posts()) : while (have_posts()) : the_post(); ?>
      <?php //get_template_part("lib/parts/content-parts"); ?>
    <?php //endwhile; endif; ?>
    </div>
  </section> -->

<?php get_footer(); ?>
