(function($){
	$(function(){
    $(".page_top").hide();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > 100) {
            $(".page_top").fadeIn("fast");
        } else {
            $(".page_top").fadeOut("fast");
        }
        scrollHeight = $(document).height();
        scrollPosition = $(window).height() + $(window).scrollTop();
        footHeight = $("footer").innerHeight();
        if ( scrollHeight - scrollPosition  <= footHeight ) {
            $(".page_top").css({
                "position":"absolute",
								"right": "20px",
                "bottom": footHeight - 20
            });
						$(".page_top").addClass("stop");
        } else {
            $(".page_top").css({
                "position":"fixed",
								"bottom": "20px",
								"right": "20px"
            });
						$(".page_top").removeClass("stop");
        }
    });
    $('.page_top').click(function () {
        $('body,html').animate({
        scrollTop: 0
        }, 400);
        return false;
    });
	});

  $(function(){

    //ドロワーメニュー
    $('.toggle_nav,.toggle_nav_bg').on('click', function () {
      $('body').toggleClass('open');
    });
    //ドロワーメニューのアコーディオン
    $('.drawer_menu a[href^=#]').click(function(){
      $(this).next('.sub-menu').slideToggle();
      $(this).toggleClass('open_menu');
      return false;//上部にスクロールされる問題を解決
    });

    //ドロワーメニュー内のアンカーリンクをクリックした時にドロワーメニューを閉じる
    $('.drawer_menu .js_close_menu').on('click', function(){
      $('body').removeClass('open');
    });
		//ドロワーメニューの子要素がある親要素にクラスを追加
		$('.drawer_menu .sub-menu').parent().addClass("li-parent");

    //スムーススクロールでドロワーメニューのアコーディオンだけ除外
    $('a[href^="#"]').not('.drawer_menu a').click(function(){
        var speed = 500;
        var href= $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top;
        $("html, body").animate({scrollTop:position}, speed, "swing");
        return false;
    });

    //グローバルナビゲーションにカレント表示する（階層構造にも対応）
    $(".header-nav__list a").each(function(){
      if(this.href == location.href) {
        $(this).parents("li").addClass("current");
      }
    });

    //PCナビのドロップダウン
    $('.header-nav__list li').hover(function(){
      $("ul:not(:animated)", this).slideDown(200);
    }, function(){
      $("ul.sub-menu",this).slideUp(200);
    });

		$( '#zip_code' ).keyup( function() {
			AjaxZip3.zip2addr( this, '', 'address','address' );
		});

    //最新の投稿
    // $('.top_kiji_ttl').matchHeight();
    // $('.top_kiji_txt').matchHeight();

    $(".custom_gallery").colorbox({
      rel:'slideshow',
      //slideshow:true,
      //slideshowSpeed:3000,
      maxWidth:"90%",
      maxHeight:"90%",
      opacity: 0.7
    });
    $(".lightbox li a").colorbox({
      rel:'slideshow',
      maxWidth:"90%",
      maxHeight:"90%",
      opacity: 0.7
    });

  });
})(jQuery)
