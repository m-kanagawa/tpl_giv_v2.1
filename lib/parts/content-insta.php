<script src="<?php echo get_template_directory_uri(); ?>/lib/js/instafeed.min.js"></script>
<script type="text/javascript">
  var feed = new Instafeed({
  target: 'instafeed',
  get: 'user',
  userId: 'xxxxxxxxxxxx',
  accessToken:'xxxxxxxxxxxx.xxxxxxxxxxxx.xxxxxxxxxxxxxxxxxxxxxxxx',
  clientId: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
  links: true,
  limit: 12,
  resolution: 'low_resolution', 
  template: '<li><a href="{{link}}" target="_blank"><span class="insta_img"><img src="{{image}}" /></span><div class="insta_options"><span class="insta_likes">{{likes}}</span><span class="insta_comments">{{comments}}</span></div></a></li>'
  });
  feed.run();
</script>
<div class="insta_box"><ul id="instafeed" class="insta_list"></ul></div>