<?php get_header(); ?>
<?php get_template_part("lib/parts/parts-h1"); ?>
<?php get_template_part("lib/parts/parts-breadcrumb"); ?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <article>
      <div class="inner post_box single_box">
        <div class="custom_content">
          <h2 class="style_ttl"><?php the_title(); ?></h2>
          <p class="data"><?php the_time('Y/m/d'); ?></p>
        </div>
        <div class="single_txt_box"><?php the_content(); ?></div>
      </div><!-- /.post -->
    </article>
    <?php endwhile; endif; ?>

    <?php get_template_part("lib/parts/content-footer"); ?>


<?php get_footer(); ?>
